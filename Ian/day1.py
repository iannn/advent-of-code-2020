from timer import timer
from aocd import data
data = set(map(int, data.splitlines()))

def summands(target):
    for i in data:
        if target - i in data:
            return i, target - i
    return None

@timer
def part1():
    comp = summands(2020)
    print(f"{comp[0]} * {comp[1]} = {comp[0] * comp[1]}")

@timer
def part2():
    for i in data:
        if (comp := summands(2020 - i)) is not None:
            print(f"{i} * {comp[0]} * {comp[1]} = {i * comp[0] * comp[1]}")
            break

part1()
part2()
