from timer import timer
from aocd import data

data = data.splitlines()

def collisions(xvelocity, yvelocity, data):
    x = count = 0
    for row in data[::yvelocity]:
        count += 1 if row[x] == "#" else 0
        x = (x + xvelocity) % len(data[0])

    return count

@timer
def part1():
    print(collisions(3, 1, data))

@timer
def part2(): # the lazy way
    print(collisions(1, 1, data) * collisions(3, 1, data) * collisions(5, 1, data) * collisions(7, 1, data) * collisions(1, 2, data))

part1()
part2()
