from timer import timer
from aocd import data
from re import split, fullmatch

data = split("[\n ]", data)
data.append("")

# these could probably be written better
FIELDS = {"byr": "19[2-9][0-9]|200[0-2]",
          "iyr": "201[0-9]|2020",
          "eyr": "202[0-9]|2030",
          "hgt": "(1[5-8][0-9]|19[0-3])cm|(59|6[0-9]|7[0-6])in",
          "hcl": "#[0-9a-f]{6}",
          "ecl": "amb|blu|brn|gry|grn|hzl|oth",
          "pid": "\d{9}"}

@timer
def part1():
    valid = total = 0
    for i in data:
        if i == '':
            total += 1 if valid == len(FIELDS) else 0
            valid = 0
            continue

        key, value = i.split(":")

        if key in FIELDS:
            valid += 1

    print(total)

@timer
def part2():
    valid = total = 0
    for i in data:
        if i == '':
            total += 1 if valid == len(FIELDS) else 0
            valid = 0
            continue

        key, value = i.split(":")
        pattern = "a^" if key not in FIELDS else FIELDS[key]

        if fullmatch(pattern, value) is not None:
            valid += 1

    print(total)

part1()
part2()
