from timer import timer
from aocd import data
from math import ceil

def normalize(steps):
    return steps.translate({ord("L"): ord("F"), ord("R"): ord("B")})

data = (normalize(line) for line in data.splitlines() if line != "")

def bpart(steps, lower, upper):
    for step in steps:
        if step == "F":
            upper = ceil((upper + lower) / 2)
        elif step == "B":
            lower += ceil((upper - lower) / 2)

    return lower, upper

data = list(map(lambda steps : bpart(steps[:7], 0, 128)[0] * 8 + bpart(steps[7:], 0, 8)[0], data))
data.sort()

print(f"Part 1: {data[::-1][0]}")

# I'm certain that there's a more efficient way to do this
for i in range(len(data) - 1):
    if data[i+1] - data[i] > 1:
        print(f"Part 2: {data[i] + 1}")
        break
