#!/usr/bin/awk -f

{ split($0, s, "[-: ]") }
(l=split(s[5], t, s[3]) - 1) >= s[1] && l <= s[2] { ++v1 }
(a=substr(s[5], s[1], 1)) != (b=substr(s[5], s[2], 1)) && (a == s[3] || b == s[3]) { ++v2 }

END { print "Part 1: ", v1; print "Part 2: ", v2 }

# $ time ./day2.awk < data
# Part 1:  493
# Part 2:  593
# 
# real	0m0.008s
# user	0m0.006s
# sys	0m0.002s
