from aocd import data
from re import findall

data = data.splitlines()

BAGS = {" ".join(rule.split()[0:2]) : findall("(\d) ([\w ]*) ", rule) for rule in data}

# Part 1

def contains(root, target):
    for qty, bag in BAGS[root]:
        if bag == target or contains(bag, target):
            return True
    return False

print(sum(contains(bag, "shiny gold") for bag in BAGS))

# Part 2

def bagsIn(bag):
    return sum(int(qty) * (bagsIn(bag) + 1) for qty, bag in BAGS[bag])

print(bagsIn('shiny gold'))
