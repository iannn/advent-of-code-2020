#!/bin/sh

infile="sample.txt"
infile="real.txt"
repeat=10
repeat=322

for line in `cat $infile`
do
    count=0
    while [ $count -ne $repeat ]
    do
        echo -e "$line\c"
        count=`expr $count + 1`
    done
    echo ""
done
