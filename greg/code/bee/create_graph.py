#!/usr/bin/python

import sys
import datetime
import plotly.graph_objects as go
import matplotlib.pyplot as plt
import numpy as np

from boto3.session import Session
import boto3

ACCESS_KEY=(sys.argv[1])
SECRET_KEY=(sys.argv[2])

epocarry=[]

ytemparry=[]
yhumarry=[]

btemparry=[]
bhumarry=[]


session = Session(aws_access_key_id=ACCESS_KEY,aws_secret_access_key=SECRET_KEY)

s3 = session.resource('s3')
your_bucket = s3.Bucket('fraize')


found=0
for s3_file in your_bucket.objects.all():
    key = s3_file.key
    if ( key == "beehivestats" ):
        fileContent = s3_file.get()['Body'].read()
        found=1

if ( found == 0 ):
    print ("Error")
    exit()

#objFile = open("infile.txt", "r")

#fileContent = objFile.read();

#objFile.close()

#print ("The text file contains: : ", fileContent.split())
for var in fileContent.split():
    type,epocstr,fenstr,cel,humstr,gpinstr=var.split(';')
    humint=int(round(float(humstr),0))
    epocint=int(round(float(epocstr),0))
    humantime = datetime.datetime.fromtimestamp(epocint).strftime('%Y-%m-%d %H:%M:%S')
    fenint=int(round(float(fenstr),0))
    epocarry.append(humantime)
    if (int(gpinstr) == 17):
        ytemparry.append(fenint)
        yhumarry.append(humint)
    elif (int(gpinstr) == 18):
        bepocarry.append(humantime)
        btemparry.append(fenint)
        bhumarry.append(humint)



fig = go.Figure()

fig.update_layout(title_text='Bee Hive Tempurture and Humidity', title_x=0.5)
fig.add_trace(go.Scatter(y=yhumarry, x=epocarry, name="Yellow Humidity",text=["tweak line smoothness<br>with 'smoothing' in line object"],hoverinfo="x+y",line_shape='spline'))
fig.add_trace(go.Scatter(y=ytemparry, x=epocarry, name="Yellow Farenheit",text=["tweak line smoothness<br>with 'smoothing' in line object"],hoverinfo="x+y",line_shape='spline'))
fig.show()
