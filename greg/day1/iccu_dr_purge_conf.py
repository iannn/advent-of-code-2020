#!/usr/bin/python2
import sys, re, os, getopt

def usage():
	print """purge_conf v0.2

-i <filename>, --input=<filename>               input file ruleset
-o <filename>, --output=<filename>              output file ruleset
-p <prefix>, --prefix=<prefix>                  prefix of elements to remove
-h, --help                                      you are here
"""

try:
	opts, args = getopt.gnu_getopt(sys.argv[1:], 'hi:o:p:', ['help', 'input=', 'output=', 'prefix='])
except getopt.GetoptError, err:
	print str(err)
	usage()
	sys.exit(2)
for o, a in opts:
	if o in ('-i', '--input'):
		inFile = a
	elif o in ('-o', '--output'):
		outFile = a
	elif o in ('-p', '--prefix'):
		prefix = a
	elif o in ('-h', '--help'):
		usage()
		sys.exit(2)
	else:
		usage()
		sys.exit(2)

error = 0

if not globals().has_key('inFile'):
	print "-i, input file required"
	error = 1
if not globals().has_key('outFile'):
	print "-o, output file required"
	error = 1
if not globals().has_key('prefix'):
	print "-p, prefix required"
	error = 1
if error == 1:
	print "\n-h for a complete list of command line options.\n"
	sys.exit(2)

try:
	inputfile = open(inFile)
except:
	print "Can't find input file: %s" % (inFile)
	sys.exit(2)

try:
	outfile = open(outFile, 'w')
except:
	print "Can't open output file: %s" % (inFile)
	sys.exit(2)

addprefix="set security zones security-zone "+sys.argv[6]
polprefix="set security policies from-zone "+sys.argv[6]

lines = inputfile.readlines()
outconf = []

for line in lines:
	address = re.search('address', line)
	if address:
		targets = re.search(addprefix, line)
		if targets:
			line = re.sub(r'^set\ ', 'delete ', line)
			outconf.append(line)	
	policy = re.search('security policies from-zone', line)
	if policy:
		targets = re.search(polprefix, line)
		if targets:
			line = re.sub(r'^set\ ', 'delete ', line)
			outconf.append(line)	

policyIDs = []

for line in outconf:
        foundID = re.search(r"policy (\d+)", line)
        if foundID:
                if foundID.group(1) not in policyIDs:
                        policyIDs.append(foundID.group(1))

cleanpolicies = []

#look for policies and simplify the output
for line in outconf:
	target = re.search(r"policy (\d+)", line)
	if target == None:
		outfile.write(line)
	else:
		newtarget = re.search(r"delete security policies from-zone (.+?) to-zone (.+?) policy (\d+)", line)
		sourceZone = newtarget.group(1)
		destZone = newtarget.group(2)
		policy = newtarget.group(3)
		newline =  "delete security policies from-zone %s to-zone %s policy %s\n" % (sourceZone, destZone, policy)
		cleanpolicies.append(newline)
		


# delete the duplicates generated from the above.
outConf = []
for element in cleanpolicies:
        if element not in outConf:
                outConf.append(element)

for line in outConf:
        outfile.write(line)
		

	
outfile.close() 
