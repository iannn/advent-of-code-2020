#!/usr/bin/python
import time
import sys
import os
import commands
import json

if len(sys.argv) != 3:
  print("Must provide a single argument with IP of vpn device")
  quit()

ip = sys.argv[1]
epoc = sys.argv[2]

def get_data(ip):
  snmpcom = "L1b3rty26"
  oid_usr = "1.3.6.1.4.1.12532.12.0"
  oid_cpu = "1.3.6.1.4.1.12532.10.0"
  oid_mem = "1.3.6.1.4.1.12532.11.0"
  oid_ive = "1.3.6.1.4.1.12532.24.0"

  device_map = {
      "10.192.229.170" : { "name" : "0055-psa-vpn-node0-mgmt", "datacenter" : "US Portsmouth" },
      "10.192.229.171" : { "name" : "0055-psa-vpn-node1-mgmt", "datacenter" : "US Portsmouth" },
      "10.192.229.172" : { "name" : "0055-psa-vpn-node2-mgmt", "datacenter" : "US Portsmouth" },
      "10.192.229.34"  : { "name" : "0055-psa-vpn-node3-mgmt", "datacenter" : "US Portsmouth" },
      "10.192.229.35"  : { "name" : "0055-psa-vpn-node4-mgmt", "datacenter" : "US Portsmouth" },
      "10.48.249.160"  : { "name" : "085a-psa-vpn-node0-mgmt", "datacenter" : "US Kansas City" },
      "10.48.249.161"  : { "name" : "085a-psa-vpn-node1-mgmt", "datacenter" : "US Kansas City" },
      "10.48.249.14"   : { "name" : "085a-psa-vpn-node2-mgmt", "datacenter" : "US Kansas City" },
      "10.48.249.33"   : { "name" : "085a-psa-vpn-node3-mgmt", "datacenter" : "US Kansas City" },
      "10.48.249.45"   : { "name" : "085a-psa-vpn-node4-mgmt", "datacenter" : "US Kansas City" },
      "168.147.31.198" : { "name" : "082s-psa-vpn-node0-mgmt", "datacenter" : "US Redmond" },
      "168.147.31.199" : { "name" : "082s-psa-vpn-node1-mgmt", "datacenter" : "US Redmond" },
      "10.127.4.76"    : { "name" : "054g-mag-vpn-node0",      "datacenter" : "GS Sydney" },
      "10.127.4.78"    : { "name" : "054g-mag-vpn-node1",      "datacenter" : "GS Sydney" },
      "10.127.68.76"   : { "name" : "068f-mag-vpn-node0",      "datacenter" : "GS Singapore" },
      "10.127.68.78"   : { "name" : "068f-mag-vpn-node1",      "datacenter" : "GS Singapore" },
      "10.126.1.13"    : { "name" : "048e-mag-vpn-node0",      "datacenter" : "GS Ash" },
      "10.126.1.15"    : { "name" : "048e-mag-vpn-node1",      "datacenter" : "GS Ash" },
      "10.126.57.25"   : { "name" : "048e-psa-vpn-node0",      "datacenter" : "GS Ash" },
      "10.126.57.26"   : { "name" : "048e-psa-vpn-node1",      "datacenter" : "GS Ash" },
      "10.137.47.178"  : { "name" : "apac-vpn-node0",          "datacenter" : "GRM Singapore" },
      "10.137.47.179"  : { "name" : "apac-vpn-node1",          "datacenter" : "GRM Singapore" },
      "10.137.111.178" : { "name" : "emea-vpn-node0",          "datacenter" : "GRM Dublin" },
      "10.137.111.179" : { "name" : "emea-vpn-node1",          "datacenter" : "GRM Dublin" },
      "10.158.12.71"   : { "name" : "sokepxx-vpn0001",         "datacenter" : "GRM Kansas City" },
      "10.158.12.72"   : { "name" : "sokepxx-vpn0002",         "datacenter" : "GRM Kansas City" },
      "10.156.12.71"   : { "name" : "sowepxx-vpn0001",         "datacenter" : "GRM Poland" },
      "10.156.12.72"   : { "name" : "sowepxx-vpn0002",         "datacenter" : "GRM Poland" },
      "10.192.229.54"  : { "name" : "0055-mag-vpn-node0",      "datacenter" : "US Portsmouth Developer"},
      "10.192.229.55"  : { "name" : "0055-mag-vpn-node1",      "datacenter" : "US Portsmouth Developer"},
      "10.48.249.96"   : { "name" : "085a-mag-vpn-node0",      "datacenter" : "US Kansas City Developer"},
      "10.48.249.98"   : { "name" : "085a-mag-vpn-node1",      "datacenter" : "US Kansas City Developer"},
      "10.128.136.18"  : { "name" : "090e-psa-vpn-node0",      "datacenter" : "GRM East-West Vietnam"},
      "10.132.216.66"  : { "name" : "010f-psa-vpn-node0",      "datacenter" : "GRM East-West Thailand"},
      "10.129.192.170" : { "name" : "030c-psa-vpn-node0",      "datacenter" : "GRM East-West Chile"},
      "10.129.192.171" : { "name" : "030c-psa-vpn-node1",      "datacenter" : "GRM East-West Chile"},
      "10.131.128.32"  : { "name" : "033e-psa-vpn-node0",      "datacenter" : "Madrid Spain" },
      "10.131.128.33"  : { "name" : "033e-psa-vpn-node1",      "datacenter" : "Madrid Spain" }
  }

  host = device_map[ip]['name']
  datacenter = device_map[ip]['datacenter']

  usr_command = "/usr/bin/snmpget -v2c -c %s %s %s 2>/dev/null | awk '{print $NF}'" % (snmpcom,ip,oid_usr)
  cpu_command = "/usr/bin/snmpget -v2c -c %s %s %s 2>/dev/null | awk '{print $NF}'" % (snmpcom,ip,oid_cpu)
  mem_command = "/usr/bin/snmpget -v2c -c %s %s %s 2>/dev/null | awk '{print $NF}'" % (snmpcom,ip,oid_mem)
  ive_command = "/usr/bin/snmpget -v2c -c %s %s %s 2>/dev/null | awk '{print $NF}'" % (snmpcom,ip,oid_ive)

  results = { 'epoc' : epoc, 'name': host, 'datacenter': datacenter }

  for loop in range(5):
    results['users']  = commands.getstatusoutput(usr_command)[1]
    results['cpu']    = commands.getstatusoutput(cpu_command)[1]
    results['memory'] = commands.getstatusoutput(mem_command)[1]
    results['swap']   = commands.getstatusoutput(ive_command)[1]

    if results['users'] != '' and results['cpu'] != '' and results['memory'] != '' and results['swap'] != '':
      return results

    time.sleep( 5 )

def send_data(data):
  log_push = '{"index" : "pulsevpn", "sourcetype" : "vpn_stats", "event" : %s}' % (json.dumps(data))
  target = open('/var/nsm_tmp/vpn_data_file_%s' % data['name'], 'w')
  target.truncate()
  target.write(log_push)
  target.close()

  #return 'curl -k -s -H "Authorization:Splunk DF82NMFASDF-88A3-WFDF-ASDF3-82A9DFLKAZDP" -d \'@/var/nsm_tmp/vpn_data_file_%s\' https://http-inputs-splunkcloud.lmig.com:443/services/collector' % data['name']
  os.system('curl -k -s -H "Authorization:Splunk DF82NMFASDF-88A3-WFDF-ASDF3-82A9DFLKAZDP" -d \'@/var/nsm_tmp/vpn_data_file_%s\' https://http-inputs-splunkcloud.lmig.com:443/services/collector' % data['name'])
  #res2 = commands.getstatusoutput('curl -k -s -H "Authorization:Splunk DF82NMFASDF-88A3-WFDF-ASDF3-82A9DFLKAZDP" -d \'@/var/nsm_tmp/np_vpn_data_file\' https://https-nonprod-logging-0055.lmig.com:443/services/collector')

data = get_data(ip)
send_data(data)
#print(send_data(data))
