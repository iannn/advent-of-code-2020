'''
Created on Dec 3, 2020

@author: n0245298
'''



puzzledata = (open('advent4.txt').read()).split("\n\n")

reqkeys = ["byr","iyr","eyr","hgt","hcl","ecl","pid"]

listofdict = []
refineddata = []
for x in puzzledata:
    _ = refineddata.append(x.replace("\n", " ").split())

for y in refineddata:
    throwaway = {}
    for z in y:
        throwaway[z[:3]] = z[4:]
    listofdict.append(throwaway)

def dictvalid1(testdict, reqkeys):
    
    for x in reqkeys:
        #print(f'testing {x}')
        if x not in testdict.keys():
            #print(f"False - missing {x}")
            return False
            break
        else:
            pass
    return True


valids = [x for x in listofdict if dictvalid1(x, reqkeys)]
print(valids)
print(f'Answer one: {len(valids)}')

def byrcheck(testdict):
    if len(testdict["byr"]) == 4 and testdict["byr"].isdecimal() and int(testdict["byr"]) >= 1920 and int(testdict["byr"]) <= 2002:
        return True
    else:
        return False

def iyrcheck(testdict):
    if len(testdict["iyr"]) == 4 and testdict["iyr"].isdecimal() and int(testdict["iyr"]) >= 2010 and int(testdict["iyr"]) <= 2020:
        return True
    else:
        return False
    

def eyrcheck(testdict):
    if len(testdict["eyr"]) == 4 and testdict["eyr"].isdecimal() and int(testdict["eyr"]) >= 2020 and int(testdict["eyr"]) <= 2030:
        return True
    else:
        return False

def hgtcheck(testdict, allowedhgt):
    
    if testdict['hgt'][-2:] in allowedhgt:
        if testdict['hgt'][-2:] == "cm" and int(testdict['hgt'][:-2]) >= 150 and int(testdict['hgt'][:-2]) <= 193:
            return True
        elif (testdict['hgt'][-2:]) == "in" and int(testdict['hgt'][:-2]) >= 59 and int(testdict['hgt'][:-2]) <= 76:
            return True
        else:
            return False
    else:
        return False

def hclcheck(testdict, allowedhcl):
    if testdict["hcl"][0] == "#":
        stringcheck = list(testdict["hcl"][1:])

        if len(stringcheck) == 6:
            for x in stringcheck:
                if x not in allowedhcl:
#                     print(f'{x} not found in allowed')
                    return False
                    break
                else:
#                     print(f'{x} found in allowed')
                    pass
            return True
        
        else:
            return False
                
    else:
        return False
    

def eclcheck(testdict, allowedeyes):
    if testdict["ecl"] in allowedeyes:
        return True
    else:
        return False

def pidcheck(testdict):
    if len(testdict["pid"]) == 9 and testdict["pid"].isdecimal():
        return True
    else:
        return False

numvalid = 0
allowedeyes = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
allowedhgt = ["cm", "in"]
allowedhcl = "0123456789abcdefABCDEF"


for passport in valids:
    byr = byrcheck(passport)
    iyr = iyrcheck(passport)
    eyr = eyrcheck(passport)
    hgt = hgtcheck(passport, allowedhgt)
    hcl = hclcheck(passport, allowedhcl)
    ecl = eclcheck(passport, allowedeyes)
    pid = pidcheck(passport)
    
    if byr and iyr and eyr and hgt and hcl and ecl and pid:
        numvalid += 1

print(numvalid)

        





    
        
            

    
    
