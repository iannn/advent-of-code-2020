'''
Created on Dec 3, 2020

@author: n0245298
'''

puzzledata = (open('advent3.txt').read()).splitlines()

rowlen = len(puzzledata[0])
rowstofinish = len(puzzledata)

def moveonce(xpos, ypos, puzzledata, xmoves, ymoves):
    newxpos = xpos + xmoves
    if (newxpos >= rowlen):
        newxpos = newxpos - rowlen
    newypos = ypos + ymoves

    try:
        if (puzzledata[newypos][newxpos]) == '#':
            return newxpos, newypos, True
        else:
            return newxpos, newypos, False
    except IndexError:
        return newxpos, newypos, False


def trajectory(xmoves, ymoves):
    startposx = startposy = 0

    collisioncount = 0

    while startposy < rowstofinish:

        onemove = moveonce(startposx, startposy, puzzledata, xmoves, ymoves)
        if onemove[2]:
            collisioncount +=1
        startposx = onemove[0]
        startposy = onemove[1]
        #print(f'Moved: X: {startposx} Y: {startposy} Treehit: {onemove[2]}')

    print(f'Trajectory Final count: {collisioncount}')
    return collisioncount


part1 = trajectory(3, 1)
print(f'Part 1 answer: {part1}')

part2 = trajectory(1, 1) * trajectory(3, 1) * trajectory(5, 1) * trajectory(7, 1) * trajectory(1, 2)
print(f'Part 2 answer: {part2}')
