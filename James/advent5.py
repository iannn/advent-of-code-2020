'''
Created on Dec 4, 2020

@author: n0245298
'''

puzzledata = (open('advent5.txt').read()).splitlines()

def makebinary(seatcode):
    temp = seatcode.replace('F', "0")
    binarydatarow = temp.replace('B', "1")
    temp2 = binarydatarow.replace('R', "1")
    binaryseatdata = temp2.replace('L', "0")

    return binaryseatdata


def getrownumber(binary):
    rownumber = 0
    for value in binary[:7]:
        rownumber = rownumber*2 + int(value)
    return rownumber


def getseatnumber(binary):
    seatnumber = 0
    for value in binary[-3:]:
        seatnumber = seatnumber*2 + int(value)
    return seatnumber

    
def getseatID(seatcode):
    binaryseating = makebinary(seatcode)
    rownumber = getrownumber(binaryseating)
    seatnumber = getseatnumber(binaryseating)
    seatID = (rownumber * 8) + seatnumber
    return seatID


def answerone(puzzledata):
    seatIDlist = []
    
    for x in puzzledata:
        _ = seatIDlist.append(getseatID(x))
    
    seatIDlist.sort(reverse=True)  
    
    return seatIDlist[0]


x = answerone(puzzledata)
print(f'Answer one is {x}')


def answertwo(puzzledata):
    
    maxseatID = getseatID('BBBBBBBRRR')
    minseatID = getseatID('FFFFFFFLLL')
    
    fullseatlist = list(range(minseatID, maxseatID))
    
    for x in puzzledata:
        _ = fullseatlist.remove(getseatID(x))
    
    for y in fullseatlist:
        position = fullseatlist.index(y)

        if fullseatlist[position + 1] == y + 1:
            continue
        else:
            return fullseatlist[y + 1]
        
            
y = answertwo(puzzledata)
print(f'Answer two is {y}')

