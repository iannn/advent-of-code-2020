'''
Created on 8 Dec 2020

@author: jamesbrett
'''


puzzledata = (open('advent8.txt').read()).splitlines()
 
dictdata = {k: v for k, v in enumerate(puzzledata)}

def domove(startline, dictdata, accumulator):
    try:
        instruction = dictdata[startline]
    except KeyError:
        return accumulator
    if instruction[:3] == 'nop':
        startline += 1
    elif instruction[:3] == 'jmp':
        if instruction [4] == '+':
            startline += int(instruction[5:])
        if instruction [4] == '-':
            startline -= int(instruction[5:])
    elif instruction[:3] == 'acc':
        if instruction [4] == '+':
            accumulator += int(instruction[5:])
            startline += 1
        if instruction [4] == '-':
            accumulator -= int(instruction[5:])
            startline += 1
   
    return startline, accumulator
 

def findtheinstruction(dictdata, searchvalue):
    matchedkey = []
    for key in dictdata.keys():
        if searchvalue in dictdata[key]:
                matchedkey.append(key)
    return matchedkey


def changethenop(dictdata, nopkey):
    dictdata[nopkey] = dictdata[nopkey].replace('nop', 'jmp')
    return dictdata


def changethejmp(dictdata, jmpkey):
    dictdata[jmpkey] = dictdata[jmpkey].replace('jmp', 'nop')
    return dictdata

## START VALUES 
accumulator = 0
startline = 0

linehit = []
process = True

allnops = findtheinstruction(dictdata, 'nop')
alljmps = findtheinstruction(dictdata, 'jmp')

## BACKUP DICT BEFORE WE MODIFY SO WE CAN COPY IT BACK
originaldict = dictdata.copy()
firstrun = True

while process:
    linehit.append(startline)
    attempt = domove(startline, dictdata, accumulator)
    if isinstance(attempt, int):
        print(f'YAY! Answer two is {accumulator}.')
        process = False
    else:
        startline = attempt[0]
        accumulator = attempt[1]

        if startline in linehit:
            if firstrun:
                print(f'YAY! Answer one is {accumulator}.')
                firstrun = False
            
            dictdata = originaldict.copy()
            linehit = []
            startline = 0
            accumulator = 0
            
            if len(allnops) > 0:
                changethisnop = allnops.pop()
                dictdata = changethenop(dictdata, changethisnop)
                continue
            
            elif len(alljmps) > 0:
                changethisjmp = alljmps.pop()
                dictdata = changethejmp(dictdata, changethisjmp)
                continue
            
            elif len(alljmps) == 0 and len(allnops) == 0:
                print('something is wrong :(')
                break
