from pprint import pprint as pp

# 1
file = open("/Users/n0191874/OneDrive - Liberty Mutual/git/python/input.txt")
list = []
for num in file:
    list.append(int(num))
file.close()
for num1 in list:
    for num2 in list:
        if num1 + num2 == 2020:
            print(num1, num2, num1 * num2)
            break

# 2
file = open("/Users/n0191874/OneDrive - Liberty Mutual/git/python/input.txt")
list = []
for num in file:
    list.append(int(num))
file.close()
for num1 in list:
    for num2 in list:
        for num3 in list:
            if num1 + num2 + num3 == 2020:
                print(num1, num2, num3, num1 * num2 * num3)
                break

# 3
valid = 0
file = open("/Users/n0191874/OneDrive - Liberty Mutual/git/python/input.txt")
for line in file:
    range = line.split()[0]
    range_low = int(range.split('-')[0])
    range_high = int(range.split('-')[1])
    letter = line.split()[1].split(':')[0]
    pw = line.split()[2]
    count = 0
    for char in pw:
        if char == letter:
            count += 1
    if range_low <= count and count <= range_high:
        valid += 1
file.close()
print(valid)

# 4
valid = 0
file = open("/Users/n0191874/OneDrive - Liberty Mutual/git/python/input.txt")
for line in file:
    positions = line.split()[0]
    char1 = int(positions.split('-')[0])
    char2 = int(positions.split('-')[1])
    letter = line.split()[1].split(':')[0]
    pw = line.split()[2]
    char1_match = False
    char2_match = False
    if pw[char1 - 1] == letter:
        char1_match = True
    if pw[char2 - 1] == letter:
        char2_match = True
    if char1_match or char2_match:
        valid += 1
    if char1_match and char2_match:
        valid -= 1
file.close()
print(valid)

# 5
file = open("/Users/n0191874/OneDrive - Liberty Mutual/git/python/input.txt")
char = 0
count = 0
skip_first_line = True
for line in file:
    if skip_first_line:
        skip_first_line = False
        continue
    char += 3
    if char >= len(line) - 1:    # end of line
        char -= 31
    # print(line[char])
    if line[char] == '#':
        count += 1
file.close()
print(count)
